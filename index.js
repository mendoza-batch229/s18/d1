console.log("Hello World!");

// [SECTION] - Parameters and Arguments
// Function in JS are lines or blocks of codes that tells our device or application to perform a certian task.
// Last session we learned how to use a basic function.

function printInput(){
	let nickname = prompt("Enter Nickname");
	console.log("Hi" + " " + nickname);
}
// printInput();

// In some cases a basic function may not be ideal
// For other cases, fucntions can also process data directly into it instead

// Consider this function
// parameter is located inside the "(parameter)" after the function -- DECLARATION
function printName(name){
	console.log("My Name is" + " " + name);
}
// argument is located inside the "(argument)" after the function -- INVOCATION
printName("Juana");
printName("John");
printName("Jane");

// Variables can also be passed as an arguments
let sampleVariable = "Yui";

printName(sampleVariable);

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of" + " " + num + " " + "divided by 8 is:" + " " + remainder);
	let isDivisibleBy8 = remainder ===0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Function as arguments

function argumentFunction(){
	console.log("These function was passed as a argument before the message was printed");

}

function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);

// Function Multiple Parameters
// Multiple "arguments" will correspond to the number of "parameters"

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);

}

createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela", " ");
createFullName("Juan", "Dela", "Cruz", "Jr");

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Parameters names are just names to refer to the argument.
// The order of the argument is the same to the order of the parameters
// baliktad and parameters susundin den ng argument kahit inayos mo pa


// The return Statement
function returnFullName(firstName, middleName, lastName){
	console.log("Test console Message");
	return firstName + " " + middleName + " " + lastName;
	console.log("This message will notbe printed");

}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);


console.log(returnFullName(firstName, middleName,lastName));

// You can also create a variable inside the fucntion to contain the result and return variable instead.

function returnAddress(city, country){
	let fullAddress = city +", "+country;
	return fullAddress;
}

let myAddress = returnAddress("Tuguegarao city", "Philippines");
console.log(myAddress);


// On the other hand when a function  only has console.log() to display its result will return undefined instead

function printPlayerInfo(username, level, job){
	console.log("username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);

}

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);

printPlayerInfo("knight_white", 95, "Paladin");